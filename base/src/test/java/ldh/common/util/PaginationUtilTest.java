package ldh.common.util;

import ldh.common.PageResult;
import org.junit.Test;

import java.util.ArrayList;

public class PaginationUtilTest {

    @Test
    public void endPage() {
        PageResult pageResult = new PageResult<>(0L, new ArrayList<String>());
        long end = PaginationUtil.getEndPage(pageResult, 1);
        System.out.println("end:" + end);
    }

    @Test
    public void endPage2() {
        long first = PaginationUtil.getFirstPage(10, 0, 10);
        System.out.println("first:" + first);
        long end = PaginationUtil.getEndPage(10, 0, 10);
        System.out.println("end:" + end);
    }
}
